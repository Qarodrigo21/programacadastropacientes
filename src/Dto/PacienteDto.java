/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dto;

/**
 *
 * @author nossa
 */
public class PacienteDto {

    private String nome_pac, sexo_pac, nasci_pac, alt_pac, end_pac, num_pac, bairro_pac, cid_pac, uf_pac, tel_pac, email_pac, pront_pac;
    private int id_aluno;

    /**
     * @return the nome_pac
     */
    public String getNome_pac() {
        return nome_pac;
    }

    /**
     * @param nome_pac the nome_pac to set
     */
    public void setNome_pac(String nome_pac) {
        this.nome_pac = nome_pac;
    }

    /**
     * @return the sexo_pac
     */
    public String getSexo_pac() {
        return sexo_pac;
    }

    /**
     * @param sexo_pac the sexo_pac to set
     */
    public void setSexo_pac(String sexo_pac) {
        this.sexo_pac = sexo_pac;
    }

    /**
     * @return the nasci_pac
     */
    public String getNasci_pac() {
        return nasci_pac;
    }

    /**
     * @param nasci_pac the nasci_pac to set
     */
    public void setNasci_pac(String nasci_pac) {
        this.nasci_pac = nasci_pac;
    }

    /**
     * @return the alt_pac
     */
    public String getAlt_pac() {
        return alt_pac;
    }

    /**
     * @param alt_pac the alt_pac to set
     */
    public void setAlt_pac(String alt_pac) {
        this.alt_pac = alt_pac;
    }

    /**
     * @return the end_pac
     */
    public String getEnd_pac() {
        return end_pac;
    }

    /**
     * @param end_pac the end_pac to set
     */
    public void setEnd_pac(String end_pac) {
        this.end_pac = end_pac;
    }

    /**
     * @return the num_pac
     */
    public String getNum_pac() {
        return num_pac;
    }

    /**
     * @param num_pac the num_pac to set
     */
    public void setNum_pac(String num_pac) {
        this.num_pac = num_pac;
    }

    /**
     * @return the bairro_pac
     */
    public String getBairro_pac() {
        return bairro_pac;
    }

    /**
     * @param bairro_pac the bairro_pac to set
     */
    public void setBairro_pac(String bairro_pac) {
        this.bairro_pac = bairro_pac;
    }

    /**
     * @return the cid_pac
     */
    public String getCid_pac() {
        return cid_pac;
    }

    /**
     * @param cid_pac the cid_pac to set
     */
    public void setCid_pac(String cid_pac) {
        this.cid_pac = cid_pac;
    }

    /**
     * @return the uf_pac
     */
    public String getUf_pac() {
        return uf_pac;
    }

    /**
     * @param uf_pac the uf_pac to set
     */
    public void setUf_pac(String uf_pac) {
        this.uf_pac = uf_pac;
    }

    /**
     * @return the tel_pac
     */
    public String getTel_pac() {
        return tel_pac;
    }

    /**
     * @param tel_pac the tel_pac to set
     */
    public void setTel_pac(String tel_pac) {
        this.tel_pac = tel_pac;
    }

    /**
     * @return the email_pac
     */
    public String getEmail_pac() {
        return email_pac;
    }

    /**
     * @param email_pac the email_pac to set
     */
    public void setEmail_pac(String email_pac) {
        this.email_pac = email_pac;
    }

    /**
     * @return the id_aluno
     */
    public int getId_aluno() {
        return id_aluno;
    }

    /**
     * @param id_aluno the id_aluno to set
     */
    public void setId_aluno(int id_aluno) {
        this.id_aluno = id_aluno;
    }

    /**
     * @return the pront_pac
     */
    public String getPront_pac() {
        return pront_pac;
    }

    /**
     * @param pront_pac the pront_pac to set
     */
    public void setPront_pac(String pront_pac) {
        this.pront_pac = pront_pac;
    }

}
