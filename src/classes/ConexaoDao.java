/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package classes;

//import com.sun.jdi.connect.spi.Connection;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author nossa
 */
public class ConexaoDao {
    
    public Connection conectaDB(){
        Connection conn = null;
        
        try {
            //String url = "jdbc:mysql://localhost:3306/diane??useSSL=false&createDatabaseIfNotExist=true";
            String url = "jdbc:mysql://localhost:3306/diane?user=root&password=";
           //String url = "jdbc:mysql://localhost:3306/diane?user=root&password=true&useSSL=false";
            //String url = "jdbc:mysql://localhost:3306/diane?verifyServerCertificate=false&useSSL=false";
            conn = (Connection) DriverManager.getConnection(url);
        } catch (SQLException erro) {
           JOptionPane.showMessageDialog(null, "ConexaoDao" + erro.getMessage());
        }
        return conn;
    }
    
}
