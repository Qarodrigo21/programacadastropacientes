/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package classes;

import Dto.PacienteDto;
import com.mysql.cj.protocol.Resultset;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 *
 * @author nossa
 */
public class PacientesDao {

    Connection conn;
    PreparedStatement pstm;
    ResultSet rs;
    ArrayList<PacienteDto> lista = new ArrayList();

    public void cadpac(PacienteDto infpacdto) {
        String sql = "insert into pacientes(Nome, Sexo, Nascimento, Altura, Endereco, Numero, Bairro, Cidade, Uf, Telefone, Email) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        conn = new ConexaoDao().conectaDB();

        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, infpacdto.getNome_pac());
            pstm.setString(2, infpacdto.getSexo_pac());
            pstm.setString(3, infpacdto.getNasci_pac());
            pstm.setString(4, infpacdto.getAlt_pac());
            pstm.setString(5, infpacdto.getEnd_pac());
            pstm.setString(6, infpacdto.getNum_pac());
            pstm.setString(7, infpacdto.getBairro_pac());
            pstm.setString(8, infpacdto.getCid_pac());
            pstm.setString(9, infpacdto.getUf_pac());
            pstm.setString(10, infpacdto.getTel_pac());
            pstm.setString(11, infpacdto.getEmail_pac());
            pstm.execute();
            pstm.close();
            
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "PacientesDao cadastro" + erro);
        }

    }

    /**
     *
     * @return
     */
    public ArrayList<PacienteDto> PesqAluno(){
        String sql = "select Id, Nome, Sexo, Telefone, prontuario from pacientes ";
        conn = new ConexaoDao().conectaDB();
        try {
             pstm = conn.prepareStatement(sql);
             rs = pstm.executeQuery();
             
             while (rs.next()){
                 PacienteDto infAluno = new PacienteDto();
                 infAluno.setId_aluno(rs.getInt("Id"));
                 infAluno.setNome_pac(rs.getString("Nome"));
                 infAluno.setSexo_pac(rs.getString("Sexo"));
                 infAluno.setTel_pac(rs.getString("Telefone"));
                 infAluno.setPront_pac(rs.getString("prontuario"));
                 
                 lista.add(infAluno);
             }
             
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "PacienteDao Pesquisa " + erro);
        }
        
        return lista;
    }

    public void alteraAluno (PacienteDto infAluno){
        String sql = "update pacientes set Nome = ?, Sexo = ?,Telefone = ?, prontuario = ? where Id = ?";
        conn = new ConexaoDao().conectaDB();
        
        try {
            
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, infAluno.getNome_pac());
            pstm.setString(2, infAluno.getSexo_pac());
            pstm.setString(3, infAluno.getTel_pac());
            pstm.setString(4, infAluno.getPront_pac());
            pstm.setInt(5, infAluno.getId_aluno());
            pstm.execute();
            pstm.close();
            
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,"PacienteDao atualiza" + erro);
        }
    }
    
    public void excluiPac(PacienteDto excluidto){
        String sql = "delete from pacientes where Id = ?";
        conn = new ConexaoDao().conectaDB();
        
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setInt(1, excluidto.getId_aluno());
            pstm.execute();
            pstm.close();
            
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "PacienteDao excluir" + erro);
        }
    }
    
    public ArrayList<PacienteDto> PesqPac(){
        String sql = "select Id, Nome, Sexo, Nascimento, Altura, Endereco, Numero, Bairro, Cidade, Uf, Telefone, Email, prontuario from pacientes ";
        conn = new ConexaoDao().conectaDB();
        try {
             pstm = conn.prepareStatement(sql);
             rs = pstm.executeQuery();
             
             while (rs.next()){
                 PacienteDto infAluno = new PacienteDto();
                 infAluno.setId_aluno(rs.getInt("Id"));
                 infAluno.setNome_pac(rs.getString("Nome"));
                 infAluno.setSexo_pac(rs.getString("Sexo"));
                 infAluno.setNasci_pac(rs.getString("Nascimento"));
                 infAluno.setAlt_pac(rs.getString("Altura"));
                 infAluno.setEnd_pac(rs.getString("Endereco"));
                 infAluno.setNum_pac(rs.getString("Numero"));
                 infAluno.setBairro_pac(rs.getString("Bairro"));
                 infAluno.setCid_pac(rs.getString("Cidade"));
                 infAluno.setUf_pac(rs.getString("Uf"));
                 infAluno.setTel_pac(rs.getString("Telefone"));
                 infAluno.setEmail_pac(rs.getString("Email"));
                 infAluno.setPront_pac(rs.getString("prontuario"));
                 
                 lista.add(infAluno);
             }
             
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "PacienteDao Pesquisa " + erro);
        }
        
        return lista;
    }
    
    public ArrayList<PacienteDto> relPac(){
        String sql = "select Id, Nome, Sexo, Nascimento, Altura, Endereco, Numero, Bairro, Cidade, Uf, Telefone, Email, prontuario from pacientes ";
        conn = new ConexaoDao().conectaDB();
        try {
             pstm = conn.prepareStatement(sql);
             rs = pstm.executeQuery();
             
             while (rs.next()){
                 PacienteDto infAluno = new PacienteDto();
                 infAluno.setId_aluno(rs.getInt("Id"));
                 infAluno.setNome_pac(rs.getString("Nome"));
                 infAluno.setSexo_pac(rs.getString("Sexo"));
                 infAluno.setNasci_pac(rs.getString("Nascimento"));
                 infAluno.setAlt_pac(rs.getString("Altura"));
                 infAluno.setEnd_pac(rs.getString("Endereco"));
                 infAluno.setNum_pac(rs.getString("Numero"));
                 infAluno.setBairro_pac(rs.getString("Bairro"));
                 infAluno.setCid_pac(rs.getString("Cidade"));
                 infAluno.setUf_pac(rs.getString("Uf"));
                 infAluno.setTel_pac(rs.getString("Telefone"));
                 infAluno.setEmail_pac(rs.getString("Email"));
                 infAluno.setPront_pac(rs.getString("prontuario"));
                 
                 lista.add(infAluno);
             }
             
             
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "PacienteDao Pesquisa " + erro);
        }
        
        return lista;
    }
   
}
